
import firebase from '../../firebase';
import React, { useState } from 'react';
//import './America.css';
import Navbar from '../../componentes/navbar';
import CardNema from '../../componentes/card';
import queryString from 'query-string';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import { Redirect } from "react-router-dom";


class america extends React.Component {

  constructor(props) {
    super(props);
    this.hosts = []
    this.state = { 
      hosts: [],
      pagesize : 12,
      pagecount :  12,
      currentPage:0
    }
  }
  handleClick(e, index) {
    
    e.preventDefault();

    this.setState({
      currentPage: index
    });
  }
  componentDidMount() {
    firebase.database().ref("/").on("value", snapshot => {
      let hostsvar = [];
      let projetonemaoide = []
      let params = queryString.parse(this.props.location.search)
      snapshot.forEach(snap => {
        if("search" in params){
            if (snap.val().Continente == "America") {
              if(params["search"] == ""){
                if (hostsvar.includes(snap.val().NgenusNspec + "/" + snap.val().Pfamily) == false) {
                  hostsvar.push(snap.val().NgenusNspec + "/" + snap.val().Pfamily);
                  projetonemaoide.push(snap.val())
                  
                }                 
              } else if (snap.val().PgenusPspec == params['search'] || snap.val().NgenusNspec == params['search']) {
                if (hostsvar.includes(snap.val().NgenusNspec + "/" + snap.val().Pfamily) == false) {
                  hostsvar.push(snap.val().NgenusNspec + "/" + snap.val().Pfamily);
                  projetonemaoide.push(snap.val())
                  
                }                
              }
            }
            
        } else if ("search" in params == false){
          
          if (snap.val().Continente == "America") {
              if (hostsvar.includes(snap.val().NgenusNspec + "/" + snap.val().Pfamily) == false) {
                hostsvar.push(snap.val().NgenusNspec + "/" + snap.val().Pfamily);
                projetonemaoide.push(snap.val())                
                
              }                
          }                               
        }   
      });
      this.setState({ hosts: projetonemaoide });
      this.setState({pagecount : Math.ceil(projetonemaoide.length / this.state.pagesize)});   

    });
  };

  render() {
    const { currentPage } = this.state;
    return (
      <>
        <Navbar />
        <div className='search'>
          <form>
            <label>
              Search:
                  <input type="text" name="search" value={this.state.value} onChange={() => { return <Redirect to="?search=" /> }} />
            </label> <t />
            <input type="submit" value="Ok" /> <t />
          </form>
        </div>


        {/* <div className='row p-3'>
      {this.state.projetonematoide.map(item => <CardNema  especie={item.NgenusNspec} host={item.PgenusPspec}/> )}
      </div> */}
        <React.Fragment>
          <div className="pagination-wrapper">
            <Pagination aria-label="Page navigation example">
              <PaginationItem disabled={currentPage <= 0}>
                <PaginationLink
                  onClick={e => this.handleClick(e, currentPage - 1)}
                  previous
                  href="#"
                />
              </PaginationItem>

              {[...Array(this.state.pagecount)].map((page, i) =>
                <PaginationItem active={i === currentPage} key={i}>
                  <PaginationLink onClick={e => this.handleClick(e, i)} href="#">
                    {i + 1}
                  </PaginationLink>
                </PaginationItem>
              )}

              <PaginationItem disabled={currentPage >= this.state.pagecount - 1}>
                <PaginationLink
                  onClick={e => this.handleClick(e, currentPage + 1)}
                  next
                  href="#"
                />
              </PaginationItem>
            </Pagination>
          </div>

          {this.state.hosts.map(item => 
                
                <div className="first col-md-3 col-sm-12" id="body">
                <div className="card-body">
                    <h5 className="card-title">Nematode Specie: {item.NgenusNspec} </h5>
                    <h6 classn="card-subtitle mb-2 text-muted">Culture : {item.PgenusPspec}</h6>
                    <div className="rodape-card">                
                        <CardNema  especie={item.NgenusNspec} host={item.PgenusPspec} data={item}/> 
                    </div>
                </div>
                </div>
            
            )
            .slice(
              currentPage * this.state.pagesize,
              (currentPage + 1) * this.state.pagesize
            )
            .map((data, i) => 
              <div className="data-slice" key={i}>
                {data}
              </div>
            )}            
        </React.Fragment>
        </>
    )
    };
}


export default america;