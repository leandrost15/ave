
import firebase from '../../firebase';
import React, {useState} from 'react';
import './inicio.css';


class inicio extends React.Component {

    constructor(props) {
        super(props);
        this.state = {projetonematoide : []
            }
        }
        
    //   componentDidMount() {     
    //       firebase.database().ref("/").limitToFirst(5).on("value", snapshot => {
    //         let projetonematoide = [];
    //         snapshot.forEach(snap => {
    //             // snap.val() is the dictionary with all your keys/values from the 'students-list' path
    //             projetonematoide.push(snap.val());
    //         });
    //         this.setState({ projetonematoide: projetonematoide });
    //       });  
    //  }
   
      render(){

      

      return (
     
        <div className="MainDiv">
         
          <div class="jumbotron text-center bg-sky">
              <h3>Teste nematodes
              </h3>
          </div>
          <div className="container">
              <table id="example" class="display table">
         
                <thead class="thead-dark">
                    <tr>
                        <th>Especie</th>
                        <th> url</th>
                        <th> Host</th>
                        
                    </tr>
                </thead>
                <tbody>
                {this.state.projetonematoide.map(data => {
                    return (
                        <tr>     
                        <td>{data.NgenusNspec}</td>
                        <td>{data.url}</td>
                        <td>{data.PgenusPspec}</td>
                        
                        </tr>
                    );
                    })} 
                </tbody>
             </table>
         </div>
        </div>
      );
    }
    
    }
  

    export default inicio;