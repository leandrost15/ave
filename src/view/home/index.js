import React, {useState, useEffect} from 'react';
import './home.css';
import {Link} from 'react-router-dom';
import Navbar from '../../componentes/navbar';
import CardNema from '../../componentes/card';
import firebase from '../../firebase';
import { Redirect } from "react-router-dom";
import queryString from 'query-string';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';

class Home extends React.Component {

    constructor(props) {
        super(props);
        this.projetonematoide = []
        this.state = {
            projetonematoide : [], //todos
            search : '',
            limit:1000,
            pagesize : 12,
            pagecount :  1,
            currentPage:0
        };
        
    }
    handleClick(e, index) {
    
        e.preventDefault();
    
        this.setState({
          currentPage: index
        });
        
      }
    
        componentDidMount() {           
            let params = queryString.parse(this.props.location.search)
            let test = []
            if("limit" in params){
                this.state.limit = parseInt(params['limit'])
            }
            firebase.database().ref("/").limitToFirst(this.state.limit).on("value", snapshot => {
                let projetonematoide = [];
                let hostsvar = [];
                snapshot.forEach(snap => {                     
                    if("continent" in params){
                        for(var key in snap.val()){                                   
                            if(params['continent'] == ""){                                
                                if (hostsvar.includes(snap.val().NgenusNspec + "/" + snap.val().Pfamily) == false) {
                                    hostsvar.push(snap.val().NgenusNspec + "/" + snap.val().Pfamily);
                                    projetonematoide.push(snap.val())
                                  }
                            } else if(snap.val()[params['continent']] == "YES" && params['continent'] != ""){
                                if (hostsvar.includes(snap.val().NgenusNspec + "/" + snap.val().Pfamily) == false) {
                                    hostsvar.push(snap.val().NgenusNspec + "/" + snap.val().Pfamily);
                                    projetonematoide.push(snap.val())
                                  }
                            }
                        }
                    } else {
                        if (hostsvar.includes(snap.val().NgenusNspec + "/" + snap.val().Pfamily) == false) {
                            hostsvar.push(snap.val().NgenusNspec + "/" + snap.val().Pfamily);
                            projetonematoide.push(snap.val())
                          }                        
                    }

                });
                let clacprojectoid = projetonematoide.filter((a, b) => projetonematoide.indexOf(a) === b)
                this.setState({ projetonematoide: clacprojectoid});        
                this.setState({pagecount : Math.ceil(clacprojectoid.length / this.state.pagesize)});    
            });
        };
    

    
    render(){  
        const { currentPage } = this.state;
    return (
        <>
        <Navbar />
        <div className='search'>
            <form>
                 <t />
                <label>
                    Continent:  
                    <select name="continent" onChange={() => {return <Redirect to="?continent=" />}}>
                        <option value="Asia" >Asia</option>
                        <option value="Africa" >Africa</option>
                        <option value="Europa" >Europe</option>
                        <option value="America" > America</option>
                        <option value="Oceania" > Oceania</option>
                    </select>                       
                </label> <t />                
                    <input type="submit" value="Search" /> <t />
                    <select name="limit" onChange={() => {return <Redirect to="?limit=" />}}>
                        <option value="100" >12 Results</option>
                        <option value="1000" >100 Results </option>
                        <option value="10000" >1000 Results</option>
                        <option value="999999" > List All</option>
                    </select>                    
            </form>
        </div>
        
        
        {/* <div className='row p-3'>
        {this.state.projetonematoide.map(item => <CardNema  especie={item.NgenusNspec} host={item.PgenusPspec}/> )}
        </div> */}
        <React.Fragment>
            <div className="pagination-wrapper">
                <Pagination aria-label="Page navigation example">
                    <PaginationItem disabled={currentPage <= 0}>
                        <PaginationLink
                            onClick={e => this.handleClick(e, currentPage - 1)}
                            previous
                            href="#"
                        />
                    </PaginationItem>

                    {[...Array(this.state.pagecount)].map((page, i) => 
                        <PaginationItem active={i === currentPage} key={i}>
                            <PaginationLink onClick={e => this.handleClick(e, i)} href="#">
                                {i + 1}
                            </PaginationLink>
                        </PaginationItem>
                    )}

                    <PaginationItem disabled={currentPage >= this.state.pagecount - 1}>
                        <PaginationLink
                            onClick={e => this.handleClick(e, currentPage + 1)}
                            next
                            href="#"
                        />                    
                    </PaginationItem>                  
                </Pagination>
            </div>   
            
            {this.state.projetonematoide.map(item => 
                
                <div className="first col-md-3 col-sm-12" id="body">
                <div className="card-body">
                    <h5 className="card-title">Nematode Specie: {item.NgenusNspec} </h5>
                    <h6 classn="card-subtitle mb-2 text-muted">Culture : {item.PgenusPspec}</h6>
                    <div className="rodape-card">                
                        <CardNema  especie={item.NgenusNspec} host={item.PgenusPspec} data={item}/> 
                    </div>
                </div>
                </div>
            
            )
            .slice(
              currentPage * this.state.pagesize,
              (currentPage + 1) * this.state.pagesize
            )
            .map((data, i) => 
              <div className="data-slice" key={i}>
                {data}
              </div>
            )}            
        </React.Fragment>
        </>
    )
    };
}

export default Home;