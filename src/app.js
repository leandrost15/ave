import React from 'react';
import {BrowserRouter as Router, Route,Switch } from 'react-router-dom';

import inicio from './view/inicio/';
import hosts from './view/Hosts/';
import nemato from './view/Nemato/';
import home from './view/home/';
import info from './view/detalhes/info';
import africa from './view/africa/'
import asia from './view/asia/'
import america from './view/america/'
import australia from './view/australia';
import europe from './view/europe';
// function App() {
//   return (
//     <Router>
//       <Route path='/txu' component={inicio} />
//       <Route path='/hosts' component={hosts} />
//       <Route path='/nemato' component={nemato} />
//       <Route path='/' component={home} />
//       <Route path='/info' component={info} />
      
//     </Router>
//   );
//}
function App() {
  return (
    <Router>
      <Switch>
      <Route path='/nemato' component={nemato} />
      <Route path='/hosts' component={hosts} />
      <Route path='/africa' component={africa} />
      <Route path='/asia' component={asia} />
      <Route path='/america' component={america} />
      <Route path='/australia' component={australia} />
      <Route path='/europe' component={europe} />
      <Route path='/' component={home} />
      
      </Switch>
      
    </Router>
  );
}
export default App;