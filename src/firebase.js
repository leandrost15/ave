import firebase from 'firebase';

// For Firebase JS SDK v7.20.0 and later, measurementId is optionalcmd
const config = {
  apiKey: "AIzaSyD6RH0m0ryCl_8b8rCFedhj7sB8CjZUQrw",
  authDomain: "realtimeproject-89f6f.firebaseapp.com",
  databaseURL: "https://realtimeproject-89f6f.firebaseio.com",
  projectId: "realtimeproject-89f6f",
  storageBucket: "realtimeproject-89f6f.appspot.com",
  messagingSenderId: "906865142502",
  appId: "1:906865142502:web:5f9fd8264973fc06a52927",
  measurementId: "G-96ST7P1779"
};

  firebase.initializeApp(config);
  export default firebase;