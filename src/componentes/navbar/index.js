import React from 'react';
import './navbar.css';
import {Link} from 'react-router-dom';

function NavBar() {
    return(
      
        <nav className="navbar navbar-expand-md">
  <Link to="/" className="navbar-brand text-haiti font-weight-bold" id="wellcome">HOME!</Link>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarsExample01" aria-expanded="false" aria-label="Toggle navigation">
        <i class="far fa-bars text-haiti" ></i>
    </button>

  <div className="collapse navbar-collapse" id="navbarNav">
    <ul className="navbar-nav mr-auto">
      <li className="nav-item">
        <Link className="nav-link" to="nemato" id="btn-nemato">Nematode's Species <span class="sr-only">(current)</span></Link>
      </li>
      <li className="nav-item">
        <Link className="nav-link" to="hosts" id="btn-nemato">Nematode's Culture </Link>
      </li>

      <li className="nav-item dropdown">
        <Link className="nav-link dropdown-toggle" to="#" id="dropdown03" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="btn-nemato">Continent</Link>
        <div className="dropdown-menu" aria-labelledby="dropdown03">
          <Link className="dropdown-item" to="africa">Africa</Link>
          <Link className="dropdown-item" to="america">America</Link>
          <Link className="dropdown-item" to="asia">Asia</Link>
          <Link className="dropdown-item" to="australia">Australia</Link>
          <Link className="dropdown-item" to="europe">Europe</Link>
          
        </div>
      </li> *
     
    </ul>
   

  </div>
</nav>


    )
}

export default NavBar;