import React, {useState, useEffect} from 'react';
import './card.css';
import {Link} from 'react-router-dom';
import { Confirm } from 'react-st-modal';
import { CustomDialog, useDialog } from 'react-st-modal';

function CustomDialogContent({host,especie,data}) {
    return (
      <div>
        <br />
        <h5>{data.PgenusPspec}</h5><br />
        <p><b>Nematode Family:</b> {data.NFamily}</p>
        <p><b>Nematode Especie:</b> {data.NgenusNspec}</p>
        <p><b>Culture Especie:</b> {data.NgenusNspec}</p>
        <p><b>Culture Family:</b> {data.Pfamily}</p>
        <p><b>Morphology and Anatomy:</b> {data.morfologiaeAnatomia}</p>
        <p><b>Continents:</b> {data.Continente}</p>
        <p><b>Economy:</b> {data.Economia}</p>
        <p><b>Damage:</b> {data.Damage}</p>
        <p><b>Management:</b> {data.Management}</p>
        <p><b>URL:</b> {data.url}</p>          
      </div>
    );
  }

function CardNema({especie, host,data}){
    return (
        <div>
          <button
            class="btn btn-secondary" 
            onClick={async () => {
              const result = await CustomDialog(
                <CustomDialogContent especie={especie} data={data} host={host}/>,
                {
                  title: especie,
                  showCloseIcon: true,
                },
              );
            }}
          >
            View details
          </button>
        </div>
      );
}



export default CardNema;



